FROM library/debian:bullseye-slim
RUN apt -y update &&\
 apt -y upgrade &&\
 apt -y install mumble-server
CMD /usr/sbin/murmurd -fg -ini /etc/mumble-server.ini
EXPOSE 64738/tcp 64738/udp
